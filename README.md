# Learning Python

A set of modules designed to give a solid introduction to Python to software engineers. These modules
assume that the participant is already versed in software terminology and has professional development experience. They
are not intended for complete beginners who are looking to learn a software language for the first time. This course
is fairly opinionated in its practices so it may not be exactly what you're looking for.

## Installation

- Clone the repository locally.
- `brew install pipenv`
- `cd /path/to/learning_python`
- `pipenv install`


# Module Zero - Koans

_/ˈkōän/ - a paradoxical anecdote or riddle, used in Zen Buddhism to demonstrate the inadequacy of logical reasoning 
and to provoke enlightenment._

In software, a koan is typically a set of failing tests that, in the process of converting to passing tests, 
teach the participant the syntax and rules of a language. They are very useful when learning a new language. Why was
the name "koan" chosen, given its definition? Who knows. You tell me.

## Intent

The purpose of this module is give you enough insight into the syntax of Python to be able to read and write it. It is
_not_ the intention of this module to make you good at Python. Think of this as one of those grammar exercises you
had to do in the 2nd grade and always failed (or maybe you were good at English class).

## Process

Take a look at the `koans` directory of this repository. You will find 17 koans in the order I think you'll find most
useful. These were all 'borrowed' from the official [Python Koans](https://github.com/gregmalcolm/python_koans)
project but with a few changes and omissions. If you get through them and want more, take a look at that repo.

Starting with `00_control_statements_test.py`, you'll want to replace any instance of `__` or `___` with an actual
value. That value should _also_ make the test pass. To run the tests, run the command `pipenv run test_koans`. Once
all the koans have passed, commit your changes (this will make the next module easier), sit back, take a break, 
and get ready for Module One.


# Module One - Randori (Greed Game)

Randori (乱取り) is a term used in Japanese martial arts to describe free-style practice. Randoris in software
were made popular by [Code Retreat](https://www.coderetreat.org/) where they are typically practiced using Conway's
Game of Life. Here were going to use a game called Greed to do our Randori.

## Intent

The purpose of randoris is _not_ to create a perfect piece of software; it's to practice with no consequence. We don't
often get to write code that doesn't matter in our jobs, so it's important that we give ourselves time to write code
where a mistake is ok. When doing a randori as part of a Code Retreat, typically everyone wipes away the whole of
their code every 45 minutes to reinforce the idea of impermanence.

We will be using this randori today to work on getting comfortable writing Python. By the end of this module, you will
have written a basic game in python, as well as written unit tests using PyTest and the language-included `unittest`
module.

## Process

Before we go too far into the randori, here are the two commands you'll want to be using:

```bash
pipenv run test_greed
pipenv run play_greed
```

There are two ways to participate in this randori, with the methods depending on whether you're doing it alone or as
part of a pairing. I'll talk about each independently:

### Solo

Start by reading the rules of the game several times over to get familiar with them. You can of course reference the
rules at any point in the process, but a base familiarity will help. Next, get out your phone/stop watch and put 45
minutes on the clock. Start the timer and begin working on implementing every rule of the game, writing first a unit
test for that rule that fails, and then making it pass by writing the necessary code. The first failing test is
provided to get you started (and its answer is commented out in the code to get you rolling). FYI, if you're not
familiar, this pattern is known as TDD and is the best way of writing code (I told you this course was opinionated).

The rules of Greed are fairly simple and are included in a text file in the `greed` directory.

### Pair

Start by reading the rules of the game out loud a few times to get familiar with them. Do actually read them aloud,
communication is a huge part of pairing and we need to get comfortable using our voices.

Start a timer for 45 minutes.

For the first pass through, we'll be doing what's called ping-pong pairing. Here's how this works:
- Person A writes a failing test that when passing will confirm the first rule of the game
- Person B writes code that makes that test pass. Person B then writes a failing test that when passing confirms
the second rule of the game
- Person A writes code that makes that test pass. Wash, rinse, repeat

Talk through each of the solutions. You're not trying to stump each other, you're trying to solve each problem you've
created together. The first failing test is provided to get you started (and its answer is commented out in the code 
to get you rolling).

### When the timer goes off

When the timer runs out, run `git checkout .` in your terminal. Yes, all your work just went away. _That's ok,_ we're
learning here. Repeat this whole process. Maybe repeat it twice. This is practice that is important and you should give
yourself plenty of time with it to get familiar. Maybe add some challenges in future iterations (can't use if 
statements, can't use loops, etc.), or try only writing functional code. The point is to play and learn, not to write
great code. After a few iterations of this, commit your final solution for posterity (or not, it doesn't matter) and get
ready for Module Two.
