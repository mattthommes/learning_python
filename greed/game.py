from random import randint
from typing import List


def request_user_input(txt: str) -> str:
    """
    Returns the user-provided input for the request text passed in
    :param txt:
    :return:
    """
    return input("\n\n" + txt + "\n\n")


def roll_dice(die_count=5) -> List:
    """
    Returns a list of the results of rolling the provided number of die
    :param die_count:
    :return:
    """
    # return [randint(1, 6) for _ in range(die_count)]
    return []


class Greed:
    def __init__(self, ask_question=request_user_input):
        self.ask_question = ask_question

    def play(self):
        roll = roll_dice()
        self.ask_question(f"You rolled {roll}. Hit enter to start.")


if __name__ == "__main__":
    game = Greed()

    game.play()
