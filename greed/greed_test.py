from koans.koan import *
from .game import *


class GreedTest(Koan):
    def test_rolling_the_dice_uses_five_die(self):
        result_of_roll = roll_dice()
        self.assertEqual(5, len(result_of_roll), "The first roll of the dice should be with 5 die")
