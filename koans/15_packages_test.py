#
# Package hierarchy of Python Koans project:
#
# contemplate_koans.py
# koans/
#     __init__.py
#     about_asserts.py
#     about_attribute_access.py
#     about_class_attributes.py
#     about_classes.py
#     ...
#     a_package_folder/
#         __init__.py
#         a_module.py

from koans.koan import *


class AboutPackages(Koan):
    def test_1_subfolders_can_form_part_of_a_module_package(self):
        # Import ./a_package_folder/a_module.py
        from .a_package_folder.a_module import Duck

        duck = Duck()
        self.assertEqual(__, duck.name)

    def test_2_subfolders_become_modules_if_they_have_an_init_module(self):
        # Import ./a_package_folder/__init__.py
        from .a_package_folder import an_attribute

        self.assertEqual(__, an_attribute)
